<?php

use App\Conference;
use Illuminate\Database\Seeder;

class ConferenceAndWorkshopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $conference = Conference::create([
            'name' => 'Workshop 1',
            'time_start' => '09:00:00',
            'time_end' => '10:00:00',
            'country' => 'CA'
        ]);

        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 1
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 2
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 3
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 4
        ]);

        $conference = Conference::create([
            'name' => 'Workshop 2',
            'time_start' => '10:00:00',
            'time_end' => '11:00:00',
            'country' => 'UK'
        ]);

        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 1
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 2
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 3
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 4
        ]);

        $conference = Conference::create([
            'name' => 'Workshop 3',
            'time_start' => '10:30:00',
            'time_end' => '12:00:00',
            'country' => 'US'
        ]);

        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 1
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 2
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 3
        ]);
        $conference->workshops()->create([
            'max_seats' => 10,
            'weekday' => 4
        ]);

        $conference = Conference::create([
            'name' => 'Presentation 1',
            'time_start' => '13:00:00',
            'time_end' => '15:00:00',
            'country' => 'CA'
        ]);

        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 1
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 2
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 3
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 4
        ]);

        $conference = Conference::create([
            'name' => 'Presentation 2',
            'time_start' => '14:45:00',
            'time_end' => '16:45:00',
            'country' => 'UK'
        ]);

        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 1
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 2
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 3
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 4
        ]);

        $conference = Conference::create([
            'name' => 'Presentation 3',
            'time_start' => '14:00:00',
            'time_end' => '17:00:00',
            'country' => 'US'
        ]);

        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 1
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 2
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 3
        ]);
        $conference->workshops()->create([
            'max_seats' => 15,
            'weekday' => 4
        ]);
    }
}
