<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conference extends Model
{

    protected $guarded = ['id'];

    public function workshops()
    {
        return $this->hasMany('App\Workshop');
    }

    /**
     * Verify is there is a workshop for this day for this conference
     * @param  int  $weekday Week Day (1=Monday)
     * @return boolean
     */
    public function hasWorkshopOnDay($weekday)
    {
        return $this->workshops()->where('weekday', $weekday)->count() > 0;
    }

    /**
     * Get workshop on the specified day or null
     * @param  int  $weekday Week Day (1=Monday)
     * @return App\Workshop|null
     */
    public function getWorkshopOnDay($weekday)
    {
        return $this->workshops()->where('weekday', $weekday)->first();
    }

    /**
     * Format time to HH:MM
     * @param  string $value Time to be transform
     * @return string        Formatted time
     */
    protected function formatTime($value)
    {
        return date('H:i', strtotime($value));
    }

    public function getTimeStartAttribute($value)
    {
        return $this->formatTime($value);
    }

    public function getTimeEndAttribute($value)
    {
        return $this->formatTime($value);
    }
}

