<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workshop extends Model
{

    /**
     * @var array
     */
    protected $guarded = ['id'];

    public function conference()
    {
        return $this->belongsTo('App\Conference');
    }

    public function attendees()
    {
        return $this->hasMany('App\Attendee');
    }

    /**
     * Return the number of seats that can be reserved
     * @return int
     */
    public function getAvailableSeats()
    {
        return $this->max_seats - $this->attendees()->where('status', 'confirmed')->count();
    }

    public function humanWeekDay()
    {
        return strftime('%A', strtotime('Sunday +'.$this->weekday.' days'));
    }

    public function hasAvailableSeats($quantity = 1)
    {
        return $this->getAvailableSeats() >= $quantity;
    }

    public function hasConflict($test)
    {
        if ($this->weekday !== $test->weekday) {
            return false;
        }

        if (strtotime($this->conference->time_start) < strtotime($test->conference->time_end) && strtotime($test->conference->time_start) < strtotime($this->conference->time_end)) {
            return true;
        }

        return false;
    }

}
