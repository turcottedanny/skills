<?php

namespace App\Http\Controllers;

use App\Conference;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostGroupBookingRequest;
use App\Http\Requests\PostIndividualBookingRequest;
use App\Workshop;

class BookingController extends Controller
{
	public function __construct()
	{
		$this->middleware('has.not.booked', ['except' => ['getConfirmation']]);
	}

    public function getIndividual()
    {
        $conferences = Conference::all();

        return view('booking.individual', compact('conferences'));
    }

    public function postIndividual(PostIndividualBookingRequest $request)
    {
        $workshops_id = $request->get('workshops');
        $workshops = Workshop::find($workshops_id);
        $conflicts = [];

        foreach($workshops as $position => $workshop)
        {
            if( ! $workshop->hasAvailableSeats(1))
            {
                return redirect()->back()->withInput();
            }

            for($i = $position+1; $i < $workshops->count(); $i++)
            {
                $test_workshop = $workshops[$i];

                if($workshop->hasConflict($test_workshop))
                {
                    if( ! in_array($workshop->id, $conflicts))
                    {
                        $conflicts[] = $workshop->id;
                    }
                    if( ! in_array($test_workshop->id, $conflicts))
                    {
                        $conflicts[] = $test_workshop->id;
                    }
                }
            }
        }

        if(count($conflicts) > 0)
        {
            \Session::flash('conflicts', $conflicts);

            return redirect()->back()->withInput();
        }

        $user = \Auth::user();
        $workshops->each(function($workshop) use ($user){
            $workshop->attendees()->create([
                'user_id' => $user->id,
                'name' => $user->name,
                'status' => 'requested',
                'country'=> $user->country
            ]);
        });

        return redirect(action('BookingController@getConfirmation'));
    }

    public function postGroup(PostGroupBookingRequest $request)
    {
        $conflicts = [];
        $attendees = [];
        foreach($request->get('attendees') as $workshopID => $workshopAttendees)
        {
            $workshop = Workshop::find($workshopID);
            foreach($workshopAttendees as $i => $attendee)
            {
                if($attendee['country'] == $workshop->conference->country)
                {
                    if(! isset($conflicts[$workshopID]))
                    {
                        $conflicts[$workshopID] = [];
                    }

                    $conflicts[$workshopID][] = $i;
                }
                else
                {
	                $attendees[] = $attendee + ['workshop' => $workshop];
                }
            }
        }

        if($conflicts)
        {
            \Session::flash('conflicts', $conflicts);

            return redirect()->back()->withInput();
        }

        $user = \Auth::user();
        foreach($attendees as $attendee)
        {
            $attendee['workshop']->attendees()->create([
                'user_id' => $user->id,
                'name' => $attendee['name'],
                'status' => 'requested',
                'country'=> $attendee['country']
            ]);
        }

        return redirect(action('BookingController@getConfirmation'));
    }

    public function getGroup()
    {
    	$days = [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday'];

    	$conferencesByDay = [];

    	foreach ($days as $day_number => $day) {
    		$conferencesByDay[$day_number] = Conference::whereHas('workshops', function ($query) use ($day_number) {
    			$query->where('weekday', $day_number);
    		})->get();
    	}

    	$countries = ['CA', 'UK', 'US'];

        return view('booking.group', compact('conferencesByDay', 'days', 'countries'));
    }

    public function getConfirmation()
    {
    	$contact = \Auth::user();

    	$attendees = $contact->attendees;

    	return view('booking.confirmation', compact('attendees', 'contact'));
    }
}
