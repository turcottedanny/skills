<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegistrationRequest;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegistrationController extends Controller
{
    public function getIndex()
    {
        return view('registration.index');
    }

    public function postIndex(RegistrationRequest $request)
    {
        $user = User::create($request->except(['agreed', 'individual_registration', 'group_registration']));

        \Auth::login($user);

        if($request->has('individual_registration'))
        {
            return redirect()->action('BookingController@getIndividual');
        }
        else
        {
            return redirect()->action('BookingController@getGroup');
        }
    }
}
