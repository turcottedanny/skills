<?php

namespace App\Http\Controllers;

use App\Attendee;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function getIndex()
    {
        $attendees = Attendee::join('workshops', 'attendees.workshop_id', '=', 'workshops.id')
            ->join('conferences', 'workshops.conference_id', '=', 'conferences.id')
            ->orderBy('weekday', 'asc')
            ->orderBy('conferences.name', 'asc')
            ->orderBy('attendees.id', 'asc')
            ->get();

        return view('admin.index', compact('attendees'));
    }

    public function getCsv()
    {
        $attendees = Attendee::select(['users.name AS user_name', 'users.organization_name', 'attendees.name', 'attendees.country', 'attendees.id', 'attendees.created_at'])
            ->join('users', 'users.id', '=', 'attendees.user_id')
            ->orderBy('id', 'asc')
            ->get();

        $path = storage_path('attendees.csv');

        $handle = fopen($path, 'w+');

        fputcsv($handle, ['Booking Number', 'Booking Contact Name', 'Booking Contact Organization', 'Attendee Name', 'Attendee Country']);

        foreach($attendees as $attendee) {
            $csv_data = ['booking_no' => $attendee->bookingNumber()] + array_except($attendee->toArray(), ['id', 'created_at']);
            fputcsv($handle, $csv_data);
        }

        fclose($handle);

        return response()->download($path);
    }

    public function getLogout()
    {
        \Auth::logout();
        return redirect(str_replace('http://', 'http://log:out@', url('admin')));
    }
}
