<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
	$conferences = App\Conference::all();
    return view('welcome', compact('conferences'));
});


Route::controller('/registration', 'RegistrationController');

Route::group(['middleware' => ['auth']], function () {
    Route::controller('/booking', 'BookingController');
});

Route::group(['middleware' => ['auth.basic', 'is.admin']], function () {
    Route::controller('/admin', 'AdminController');
});