<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendee extends Model
{

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function workshop()
    {
        return $this->belongsTo('App\Workshop');
    }

    public function bookingNumber()
    {
        return $this->created_at->format('Y').str_pad($this->id, 4, '0', STR_PAD_LEFT);
    }
}
