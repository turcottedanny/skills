@extends('layouts.master')

@section('content')
<h1>Booking</h1>
<?php
    $old_workshops = old('workshops') ?: [];
    $conflicts = \Session::has('conflicts') ? \Session::get('conflicts') : [];
?>

@if (count($errors) > 0 || \Session::has('conflicts'))
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            @if(\Session::has('conflicts'))
                <li>There's hours conflicts</li>
            @endif
        </ul>
    </div>
@endif

<form action="/booking/individual" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <table class="border">
        <thead>
            <th>Session</th>
            <th colspan="2">Monday</th>
            <th colspan="2">Tuesday</th>
            <th colspan="2">Wednesday</th>
            <th colspan="2">Thursday</th>
        </thead>
        <tbody>
            @foreach ($conferences as $conference)
                <tr>
                    <th>
                        {{ $conference->name }} <br/>
                        {{ $conference->time_start }} - {{ $conference->time_end }}
                    </th>
                    @for ($i = 1; $i <= 4; $i++)
                        @if ($conference->hasWorkshopOnDay($i))

                            <td>Available {{ $conference->getWorkshopOnDay($i)->getAvailableSeats() }}</td>
                            <td {!! in_array($conference->getWorkshopOnDay($i)->id, $conflicts) ? 'style="background:red;"' : '' !!}>
                                <input name="workshops[]" value="{{$conference->getWorkshopOnDay($i)->id}}" type="checkbox" {{ in_array($conference->getWorkshopOnDay($i)->id, $old_workshops) ? 'checked="checked"' : '' }}>
                            </td>
                        @else
                            <td colspan="2">Not available</td>
                        @endif
                    @endfor
                </tr>
            @endforeach
        </tbody>
    </table>
    <input type="submit" value="Submit"/>
</form>
@endsection