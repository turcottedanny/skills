@extends('layouts.master')

@section('content')
<h1>Booking</h1>
@if (count($errors) > 0 || \Session::has('conflicts'))
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            @if(\Session::has('conflicts'))
                    <li>You can't see conference if your from the country of the presenter</li>
            @endif
        </ul>
    </div>
@endif

<table>
    <thead>
    <th>
        <button class="switcher">Monday</button>
        <button class="switcher">Tuesday</button>
        <button class="switcher">Wednesday</button>
        <button class="switcher">Thursday</button>
    </th>
    </thead>
</table>

<form action="/booking/group" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    @foreach ($days as $day_number => $day)
        <div class="tab">
            <h2>{{ $day }}</h2>
            <table class="border">
                <thead>
                    <th>Session</th>
                    <th colspan="2">Seats Available</th>
                    <th>Name</th>
                    <th>Country</th>
                </thead>
                <tbody>
                    @foreach ($conferencesByDay[$day_number] as $conference)
                        <tr>
                            <th>
                                {{ $conference->name }}
                                {{ $conference->time_start }} - {{ $conference->time_end }}
                            </th>
                            <td>Available {{ $conference->getWorkshopOnDay($day_number)->getAvailableSeats() }}</td>
                            <td>
                                <select class="seats" data-workshop-id="{{ $conference->getWorkshopOnDay($day_number)->id }}">
                                    @for ($i = 0; $i <= $conference->getWorkshopOnDay($day_number)->getAvailableSeats(); $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </td>
                            <td id="inputs_name_{{ $conference->getWorkshopOnDay($day_number)->id }}"></td>
                            <td id="selects_country_{{ $conference->getWorkshopOnDay($day_number)->id }}"></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @endforeach
    <input type="submit" value="Book"/>
</form>
<script type="application/javascript">
window.addEventListener('load', function(){
    var countries = {!! json_encode($countries) !!},
        conflicts = {!! json_encode(\Session::has('conflicts') ? \Session::get('conflicts') : []) !!},
        old_attendees = JSON.parse('{!! json_encode(old('attendees')) !!}'),
        selectsSeats = document.querySelectorAll('.seats'),
        switchers = document.querySelectorAll('.switcher'),
        tabs = document.querySelectorAll('.tab');
    [].forEach.call(tabs, function(tab){
        tab.style.display = 'none';
    });
    tabs[0].style.display = 'block';

    [].forEach.call(switchers, function(switcher, index){
        switcher.addEventListener('click', function(){
            [].forEach.call(tabs, function(tab){
                tab.style.display = 'none';
            });

            tabs[index].style.display = 'block';
        });
    });

    for(var workshopID in old_attendees){
        var namesZone = document.getElementById('inputs_name_' + workshopID),
            countriesZone = document.getElementById('selects_country_' + workshopID),
            nb_attendee = 0;

        for(var index in old_attendees[workshopID]){
            var attendee = old_attendees[workshopID][index];
            intWorkshopID = parseInt(workshopID);

            namesZone.appendChild(createInput(intWorkshopID, attendee['name'], index));
            namesZone.appendChild(generateBreakLine());
            countriesZone.appendChild(createSelect(intWorkshopID, attendee['country'], index, workshopID in conflicts && conflicts[workshopID].indexOf(parseInt(index)) > -1));
            countriesZone.appendChild(generateBreakLine());
            nb_attendee++;
        }
        document.querySelector('[data-workshop-id="'+workshopID+'"] option[value="'+nb_attendee+'"]').setAttribute('selected', true);
    }

    function onChangeSeats(select){
        select.addEventListener('change', function(){
            var seatsQuantity = this.value,
                workshopID = this.getAttribute('data-workshop-id');

            fillZonesOnChange(workshopID, seatsQuantity);
        });
    }

    function createInput(workshopID, value, index){
        var input = document.createElement('input');
        input.name = 'attendees['+workshopID+']['+index+'][name]';
        value && (input.value = value);
        return input;
    }

    function createSelect(workshopID, value, index, hasConflict){
        var select = document.createElement('select');

        select.name = 'attendees['+workshopID+']['+index+'][country]';
        if(hasConflict){
            select.style.backgroundColor = 'red';
        }

        countries.forEach(function(country){
            var option = document.createElement('option');
            option.value = country;
            option.textContent = country;
            select.appendChild(option);
            value === country && (option.setAttribute('selected', true));
        });

        !value && (select.firstChild.setAttribute('selected', true));

        return select;
    }

    function generateBreakLine(){
        return document.createElement('br');
    }

    function fillZonesOnChange(workshopID, quatity){
        var namesZone = document.getElementById('inputs_name_' + workshopID),
            countriesZone = document.getElementById('selects_country_' + workshopID);

        namesZone.innerHTML = '';
        countriesZone.innerHTML = '';

        while(quatity--){
            namesZone.appendChild(createInput(workshopID, null, quatity));
            namesZone.appendChild(generateBreakLine());
            countriesZone.appendChild(createSelect(workshopID, null, quatity));
            countriesZone.appendChild(generateBreakLine());
        }
    }

    [].forEach.call(selectsSeats, onChangeSeats);
});
</script>
@endsection
