@extends('layouts.master')

@section('content')
<h1>Confirmation</h1>
<p>Your request has been posted, you'll receive a confirmation email soon.</p>

<h2>Booking Contact</h2>
<table class="border">
	<tr>
		<th>Name</th>
		<td>{{ $contact->name }}</td>
	</tr>
	<tr>
		<th>Email</th>
		<td>{{ $contact->email }}</td>
	</tr>
	<tr>
		<th>Organization</th>
		<td>{{ $contact->organization_name }}</td>
	</tr>
	<tr>
		<th>Phone</th>
		<td>{{ $contact->phone }}</td>
	</tr>
	<tr>
		<th>Country</th>
		<td>{{ $contact->country }}</td>
	</tr>
</table>

<h2>Attendees</h2>
<table class="border">
	<thead>
		<th>Day</th>
		<th>Session</th>
		<th>Booking Number</th>
		<th>Attendees</th>
		<th>Status</th>
	</thead>
	<tbody>
		@foreach ($attendees as $attendee)
			<tr>
				<td>{{ $attendee->workshop->humanWeekDay() }}</td>
				<td>{{ $attendee->workshop->conference->name }} {{ $attendee->workshop->conference->time_start }} {{ $attendee->workshop->conference->time_end }}</td>
				<td>{{ $attendee->bookingNumber() }}</td>
				<td>{{ $attendee->name }}</td>
				<td>{{ $attendee->status }}</td>
			</tr>
		@endforeach
	</tbody>
</table>
@endsection