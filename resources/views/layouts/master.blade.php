<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                font-family: 'arial', 'sans-serif';
            }

            .container {
                margin: 0 auto;
                width: 960px;
            }

            input, select {
                margin: 4px 0px;
            }

            .border {
                border-collapse: collapse;
            }

            .border td,
            .border th {
                padding: 4px;
                border: 1px solid #dedede;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                @yield('content')
            </div>
        </div>
    </body>
</html>
