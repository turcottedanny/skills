@extends('layouts.master')

@section('content')
<h1>Administration</h1>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<table class="border">
	<thead>
		<th>Day</th>
		<th>Session</th>
		<th>Booking Number</th>
		<th>Attendees</th>
		<th>Status</th>
		<th>Action [Confirm - Deny - Reschedule]</th>
	</thead>
	<tbody>
		@foreach ($attendees as $attendee)
			<tr>
				<td>{{ $attendee->workshop->humanWeekDay() }}</td>
				<td>{{ $attendee->workshop->conference->name }} {{ $attendee->workshop->conference->time_start }} {{ $attendee->workshop->conference->time_end }}</td>
				<td>{{ $attendee->bookingNumber() }}</td>
				<td>{{ $attendee->name }}</td>
				<td>{{ $attendee->status }}</td>
				<td>
					<label><input type="radio" name="action[{{ $attendee->id }}]" value="confirm" /> Confirm</label>
					<label><input type="radio" name="action[{{ $attendee->id }}]" value="deny" /> Deny</label>
					<label><input type="radio" name="action[{{ $attendee->id }}]" value="reschedule" /> Reschedule</label>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>

<a href="{{ action('AdminController@getCsv') }}">Export CSV</a>
@endsection