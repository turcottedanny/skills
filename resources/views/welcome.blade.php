@extends('layouts.master')

@section('content')
    <h1>Our conferences</h1>
    <table class="border">
        @foreach($conferences as $conference)
            <tr>
                <th>{{ $conference->name }}</th>
                <td>{{ $conference->time_start }}-{{ $conference->time_end }}</td>
            </tr>
        @endforeach
    </table>
    <a href="{{ url('registration') }}">Register</a>

@endsection