@extends('layouts.master')

@section('content')
<h1>Registration</h1>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ action('RegistrationController@postIndex') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"><br/>
    <input type="text" placeholder="Name" name="name" value="{{old('name')}}"/><br/>
    <input type="text" name="organization_name" placeholder="Organization" value="{{old('organization_name')}}"/><br/>
    <input type="text" name="email" placeholder="Email" value="{{old('email')}}"/><br/>
    <input type="text" name="phone" placeholder="Phone" value="{{old('phone')}}"/><br/>
    <input type="text" name="country" placeholder="Country" value="{{old('country')}}"/><br/>
    <label for="agreed"><input type="checkbox" name="agreed"/>I agree to the Rules and Regulations</label><br/>
    <input type="submit" name="individual_registration" value="Individual Registration"/><br/>
    <input type="submit" name="group_registration" value="Group Registration"/><br/>
</form>
@endsection